# Reverse Polish Notation Calculator

This app is a command line interface to a Reverse Polish Notation Calculator

## Using the application

The app uses Java SE 8. It's built with maven (v3.5.3) and will need internet connection to download the required libraries. 

### Build

Download the source code to your local machine.
Through Terminal or Command Prompt, navigate to the directory and execute the following command to build the jar file

```
mvn clean package
```
This will create an executable jar inside the /target folder.

### Execution

Through Terminal or Command Prompt, navigate to /target folder and execute the following command to start the application

```
java -jar rpn.calculator-0.0.1-SNAPSHOT-jar-with-dependencies.jar
```

Your Reverse Polish Notation Calculator is ready for use!
To quit, type in "q" or Ctrl+C and press enter.

## Unit Tests

Test cases are located in the RpnCalculatorTest.java class

## Authors

* **Bhanu Chiguluri (chiguluribs@gmail.com)**


